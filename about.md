# tech stack

* [hardware](https://dmesgd.nycbug.org/index.cgi?fts=eduard)
* NixOS for desktop
* OpenBSD for everything else
  * xterm, dwm, dmenu etc
* [dotfiles](https://codeberg.org/qdp/dotfiles)
* [Helix](https://helix-editor.com/) for editing
* [fish](https://fishshell.com/) for commands
* [syncthing](https://syncthing.net/) for syncting
* [Unison](https://www.unison-lang.org/) for relaxation
* Rust for (any) useful stuff
* C++ for living expenses
* [LogSeq](https://logseq.com/) for note-taking
