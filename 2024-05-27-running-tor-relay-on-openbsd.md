# running a Tor guard relay on OpenBSD and Vultr

## Why?

* easy way to give back to community
  * bandwidth
  * non-Linux relay improves diversity https://blog.torproject.org/new-guide-running-tor-relay/
* good learning experience
* support alternative cloud providers

## Requirements

Tor uses a lot of bandwidth therefore you want to optimise for that.

A guard relay needs at least a constat 2MB/s download & upload.  Note that Vultr (like most cloud providers) only charge for egress.  So `2*60*60*24*31/1024/1024 ~= 5.1TB`.  Vultr provides 2TB free of bandwith.

* Get two AMD High Performance - $12/month for 4TB
* You can use one of them as a personal VM / hosting - link to my stack (Syncthing)
* Install OpenBSD
* choose a random string of letters for a username and a random port between 10000 and 600000 TODO: random.org
* Configure Tor to use between 4.2MB/s and 4.6MB/s
* Set max monthly bandwidth to 6000MB/s - if configured correctly you should never exceed that.
* https://community.torproject.org/relay/setup/guard/openbsd/

## Wembaster stuff

* reload daemon on failure
* blacklist on iptables
* contribute logs to abuseip
* keep a tmux session open
* host a hidden service!
* consider how to receive notifications when the server is down
* backup they private key
